utf8 = require 'utf8'
json = require 'json'

require 'app'
require 'test'

require 'keychord'
require 'button'

require 'main_tests'

-- delegate most business logic to a layer that can be reused by other projects
require 'edit'
Editor_state = {}

-- called both in tests and real run
function App.initialize_globals()
  -- tests currently mostly clear their own state

  Global_state = {manifest={}}
  Error = nil

  -- a few text objects we can avoid recomputing unless the font changes
  Text_cache = {}

  -- blinking cursor
  Cursor_time = 0

  -- for hysteresis in a few places
  Last_resize_time = App.getTime()
  Last_focus_time = App.getTime()  -- https://love2d.org/forums/viewtopic.php?p=249700
  Last_error_check_time = App.getTime()
end

-- called only for real run
function App.initialize(arg)
  love.keyboard.setTextInput(true)  -- bring up keyboard on touch screen
  love.keyboard.setKeyRepeat(true)

  love.graphics.setBackgroundColor(1,1,1)

  initialize_default_settings()
  Text.redraw_all(Editor_state)

  love.window.setTitle('driver')

  if rawget(_G, 'jit') then
    jit.off()
    jit.flush()
  end
end

function initialize_default_settings()
  local font_height = 20
  love.graphics.setFont(love.graphics.newFont(font_height))
  local em = App.newText(love.graphics.getFont(), 'm')
  initialize_window_geometry(App.width(em))
  Editor_state = edit.initialize_state(Margin_top, Margin_left, App.screen.width-Margin_right)
  Editor_state.font_height = font_height
  Editor_state.line_height = math.floor(font_height*1.3)
  Editor_state.em = em
  Editor_state.top = 5+Editor_state.line_height+5
end

function initialize_window_geometry(em_width)
  -- maximize window
  love.window.setMode(0, 0)  -- maximize
  App.screen.width, App.screen.height, App.screen.flags = love.window.getMode()
  -- shrink height slightly to account for window decoration
  App.screen.height = App.screen.height-100
  App.screen.width = 40*em_width
  App.screen.flags.resizable = true
  App.screen.flags.minwidth = math.min(App.screen.width, 200)
  App.screen.flags.minheight = math.min(App.screen.width, 200)
  love.window.setMode(App.screen.width, App.screen.height, App.screen.flags)
end

function App.resize(w, h)
--?   print(("Window resized to width: %d and height: %d."):format(w, h))
  App.screen.width, App.screen.height = w, h
  Text.redraw_all(Editor_state)
  Editor_state.selection1 = {}  -- no support for shift drag while we're resizing
  Editor_state.right = App.screen.width-Margin_right
  Editor_state.width = Editor_state.right-Editor_state.left
  Text.tweak_screen_top_and_cursor(Editor_state, Editor_state.left, Editor_state.right)
  Last_resize_time = App.getTime()
end

function App.draw()
  Global_state.button_handlers = {}
  edit.draw(Editor_state)
  draw_menu()
  draw_error()
end

function draw_menu()
  local x = 5
  local quit_text_width = App.width(to_text('quit'))
  local quit_button_width = 2+quit_text_width+2
  button(Global_state, 'quit', {x=x, y=5, w=quit_button_width, h=Editor_state.line_height, color={0.5,0.5,0.5},
    icon = function(button_params)
             local x,y = button_params.x, button_params.y
             App.color{r=1, g=1, b=1}
             love.graphics.print('quit', 2+x, y)
           end,
    onpress1 = function()
                 send('QUIT')
               end,
  })
  x = x + quit_button_width + 15
  local manifest_text_width = App.width(to_text('manifest'))
  local manifest_button_width = 2+manifest_text_width+2
  button(Global_state, 'manifest', {x=x, y=5, w=manifest_button_width, h=Editor_state.line_height, color={0.5,0.5,0.5},
    icon = function(button_params)
             local x,y = button_params.x, button_params.y
             App.color{r=1, g=1, b=1}
             love.graphics.print('manifest', 2+x, y)
           end,
    onpress1 = function()
                 Global_state.manifest = {}
                 send('MANIFEST')
                 local response_string
                 repeat
                   love.timer.sleep(0.01)
                   response_string = receive()
                 until response_string
                 local response = json.decode(response_string)
                 for name in pairs(response) do
                   if name ~= 'parent' then
                     table.insert(Global_state.manifest, name)
                    end
                 end
               end,
  })
  x = x + manifest_button_width + 15
  for _,name in ipairs(Global_state.manifest) do
    -- button to show definition for a name
    local button_width = 2+App.width(to_text(name))+2
    button(Global_state, name, {x=x, y=5, w=button_width, h=Editor_state.line_height, color={0.5,0.5,0.5},
      icon = function(button_params)
        local x,y = button_params.x, button_params.y
        App.color{r=1, g=1, b=1}
        love.graphics.print(name, 2+x, y)
      end,
      onpress1 = function()
        send('GET '..name)
        local response_string
        repeat
          love.timer.sleep(0.01)
          response_string = receive()
        until response_string
        Editor_state.lines = {}
        for line in response_string:gmatch("[^\r\n]+") do
            table.insert(Editor_state.lines, {data=line})
        end
        Text.redraw_all(Editor_state)
      end,
    })
    x = x + button_width
    -- button to delete definition for a name
    button_width = 2+App.width(to_text('x'))+2
    button(Global_state, name..'-delete', {x=x, y=5, w=button_width, h=Editor_state.line_height, color={0.8,0,0},
      icon = function(button_params)
        local x,y = button_params.x, button_params.y
        App.color{r=1, g=1, b=1}
        love.graphics.print('x', 2+x, y)
      end,
      onpress1 = function()
        send('DELETE '..name)
      end,
    })
    x = x + button_width + 15
  end
end

function draw_error()
  if not Error then return end
  -- overlay error message at bottom
  local height = math.min(20*Editor_state.line_height, App.screen.height*0.4)
  App.color{r=0.8,g=0,b=0}
  love.graphics.rectangle('fill', 0, App.screen.height - height-10, App.screen.width, height+10)
  App.color{r=0,g=0,b=0}
  love.graphics.print(Error, 30, App.screen.height - height)
end

function App.update(dt)
  Cursor_time = Cursor_time + dt
  -- some hysteresis while resizing
  local t = App.getTime()
  if t < Last_resize_time + 0.1 then
    return
  end
  -- continually check for errors
  if t > Last_error_check_time + 0.1 then
    receive()  -- ignore anything but errors; if we are expecting something else the individual commands will wait for it
    Last_error_check_time = t
  end
  edit.update(Editor_state, dt)
end

function love.quit()
  edit.quit(Editor_state)
end

function App.mousepressed(x,y, mouse_button)
  Cursor_time = 0  -- ensure cursor is visible immediately after it moves
  if mouse_press_consumed_by_any_button_handler(Global_state, x,y, mouse_button) then
    -- press on a button and it returned 'true' to short-circuit
    return
  end
  return edit.mouse_pressed(Editor_state, x,y, mouse_button)
end

function App.mousereleased(x,y, mouse_button)
  Cursor_time = 0  -- ensure cursor is visible immediately after it moves
  return edit.mouse_released(Editor_state, x,y, mouse_button)
end

function App.focus(in_focus)
  if in_focus then
    Last_focus_time = App.getTime()
  end
end

function App.textinput(t)
  -- ignore events for some time after window in focus
  if App.getTime() < Last_focus_time + 0.01 then
    return
  end
  Cursor_time = 0  -- ensure cursor is visible immediately after it moves
  return edit.textinput(Editor_state, t)
end

function App.keychord_pressed(chord, key)
  -- ignore events for some time after window in focus
  if App.getTime() < Last_focus_time + 0.01 then
    return
  end
  Cursor_time = 0  -- ensure cursor is visible immediately after it moves
  return edit.keychord_pressed(Editor_state, chord, key)
end

function App.keyreleased(key, scancode)
  -- ignore events for some time after window in focus
  if App.getTime() < Last_focus_time + 0.01 then
    return
  end
  Cursor_time = 0  -- ensure cursor is visible immediately after it moves
  return edit.key_released(Editor_state, key, scancode)
end

-- use this sparingly
function to_text(s)
  if Text_cache[s] == nil then
    Text_cache[s] = App.newText(love.graphics.getFont(), s)
  end
  return Text_cache[s]
end
