-- the communication channel with the app
-- commands are processed on the other end, in the app

function send_everything_to_app(State)
  send(to_string(State))
end

function send(msg)
  Error = nil
  local f = io.open(love.filesystem.getUserDirectory()..'/_love_akkartik_app', 'w')
  if f == nil then return end
  f:write(msg)
  f:close()
  print('$'..color(--[[bold]]1, --[[blue]]4))
  print(msg)
  print(reset_terminal())
end

-- look for a message from outside, and return nil if there's nothing
-- if there's an error, save it and return ''
function receive()
  local f = io.open(love.filesystem.getUserDirectory()..'/_love_akkartik_driver')
  if f == nil then return nil end
  local result = f:read('*a')
  f:close()
  if result == '' then return nil end  -- empty file == no message
  print('=>'..color(0, --[[green]]2))
  print(result)
  print(reset_terminal())
  if result:match('^ERROR ') then
    Error = result:sub(#('ERROR '))
    result = ''
  end
  -- we can't unlink files, so just clear them
  local clear = io.open(love.filesystem.getUserDirectory()..'/_love_akkartik_driver', 'w')
  clear:close()
  return result
end

-- args:
--   format: 0 for normal, 1 for bold
--   color: 0-15
function color(format, color)
  return ('\027[%d;%dm'):format(format, 30+color)
end

function reset_terminal()
  return '\027[m'
end

function to_string(State)
  return table.concat(map(State.lines, function(line) return line.data end), '\n')
end

function map(arr, f)
  local result = {}
  for _, x in ipairs(arr) do
    table.insert(result, f(x))
  end
  return result
end
