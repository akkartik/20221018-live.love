# An experiment in building live apps using LÖVE

An example repo for building live programs that can be edited while they're
running.

As seen at https://spectra.video/w/wkDB5fsjBNBbsqKXGhGzwT

There are many ways to create live programs. I like this approach because of
how little code it requires, and how little software it depends on, all the
way down to the OS.

## Getting started

Download LÖVE for your platform from https://love2d.org and follow your OS's
instructions to install it. (~5MB)

```
$ git clone https://codeberg.org/akkartik/20221018-live.love live
```

There are two separate LÖVE programs here, the live `app/` and the `driver/`
for making changes to its code. I suggest you run each program from a
(separate) terminal. Both sides emit a log of the communications as seen by
them, and that can be helpful in debugging any (inevitable) issues.

```
# terminal 1
$ cd live
$ love driver
```

```
# terminal 2
$ cd live
$ love app
```

To quit, just close the windows. Everything should continually autosave.

If you ever end up with an error, the `app/` window will go unresponsive. If
you want to quit at that point without fixing the error, hit the `quit` button
in the driver window.

You should know that the two programs communicate using two files (one for
each direction) in your user folder/home directory (`_love_akkartik_*`).
Apologies for the cruft; I don't like throwing files in your space, but this
is the only cross-platform way I've been able to come up with so far. It is
always safe to delete these files after closing the driver and app. They only
ever contain ephemeral communications. Changes you make to the app are stored
elsewhere and in a more idiomatic location (https://love2d.org/wiki/love.filesystem.getSaveDirectory)

## Coding

To get a list of global bindings in the app (with some caveats; see below),
hit the `manifest` button on the driver. It'll add buttons to the menu for
top-level bindings. (This is extremely rough UI; I have no illusions that it's
the right way to go.)

There's a single buffer for editing. There's no save. This is not an editor.
The repository for code lives on the other side, in the app. You save things
by hitting `<F4>` and sending them over to the app side.

To replace the buffer's contents with the current code for a binding (function
or variable), click on its button.

_Important gotcha:_ There's no save. If you make edits but don't hit `<F4>`,
they'll be lost when you click on a different function. This too can/will be
improved.

Don't edit multiple top-level bindings at once. This repo doesn't disallow it,
but isn't designed for it either. Create or edit one binding at a time.

Every time you hit `<F4>`, the app loads the code you send it in memory, and
also saves it to a new file on disk. All such files have an incrementing
numeric prefix. Each such file contains a single top-level Lua definition. You
always have all version history saved as you program. (As long as you hit
`<F4>`.) There isn't good UI yet for browsing/modifying version history, but
the data's all there.

To delete a binding from memory, hit the red `x` next to its name in the
driver. As before, deleted bindings remain in version history on disk.

## Publishing

New versions of definitions are written in the _save directory_
(https://love2d.org/wiki/love.filesystem.getSaveDirectory). To publish them:
  * Move all versions (files with numeric prefixes) from the save directory
    into the repo. There are expected to be no collisions.
  * Replace the file `head` with the copy from the save directory.

## Existing apps

I haven't yet tried to turn existing LÖVE apps into live programs, but I
_have_ tried to design for this possibility, and I hope to demonstrate it
soon. The app repo currently has only one .lua file outside of the version
history: main.lua. However there's no reason there can't be others.

The major drawback of code that doesn't live in version history: it can't yet
be loaded from the driver. The driver only knows to pick a file with a numeric
prefix and grab its entire contents. It doesn't parse any Lua yet. However,
there's nothing stopping you from having .lua files open in some other editor
and copy-pasting them into the driver before making changes to them. Once you
make the first edit to a definition through the driver it'll be visible in the
manifest, and you'll be able to load it by name.
